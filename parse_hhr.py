#!/bin/python

# This script parses the output of hhblits to a fasta formatted 
# pairwise alignment between the input query sequence "Q" and the 
# input sequence/pdb id "T"

import sys
import os
import re
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio import SeqIO

if len(sys.argv) < 3:
    print "Usage: %s <input hhr file> <template id>" % sys.argv[0]
    sys.exit(0)

hhr = sys.argv[1]
hhrfile = open(hhr)
tempid = sys.argv[2].strip()
tempid = tempid.lower()

hhr = os.path.basename(hhr)
outfilename = hhr.split('.')[0] + '_' + tempid + '.fasta'


query = []
template = []
interest = False

def constructSequence(list):
    sequence = ''
    seqid = ''
    for elem in list:
        if (elem[2:9].startswith('ss_pred')):
            pass # ignore secondary structure prediction
        elif (elem[2:9].startswith('Consens')):
            pass # ignore Consensus sequence
        elif (elem[2:9].startswith('ss_dssp')):
            pass
        else:
            elemlist = elem.split()
            if re.match('[A-Z]', elemlist[3]):
                sequence += elemlist[3]
                seqid = elemlist[1]
            else:
                print "Error constructing sequence"
    seq = Seq(sequence)
    rec = SeqRecord(seq, id=seqid)
    return rec

lines = hhrfile.readlines()
description =''
for line in lines:
    line = line.strip()
    if(re.match('>.*' + tempid, line)):
        interest = True
        firstword = line.split()[0]
        description = line[len(firstword)::]
    elif(interest):
        if(line.startswith('>')):
            break
        elif(line.startswith('Q')):
            query.append(line)
        elif(line.startswith('T')):
            template.append(line)

queryseqrec = constructSequence(query)
queryseqrec.description = ''

templateseqrec = constructSequence(template)
templateseqrec.description = description
templateseqrec.id = tempid

print "Writing sequence records to fasta file %s" % outfilename
SeqIO.write([queryseqrec, templateseqrec], outfilename, "fasta")

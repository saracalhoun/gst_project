#!/bin/python

# Last edit April 2, 2013

#from Bio.SeqIO import *
from modeller import *
from Bio.SeqRecord import SeqRecord
from Bio.Seq import MutableSeq, Seq

class MyAlignment(object):

    def __init__(self, template=None, target=None):
        if template is not None:
            if isinstance(template.seq, Seq):
                template.seq = template.seq.tomutable()
        if target is not None:
            if isinstance(target.seq, Seq):
                target.seq = target.seq.tomutable()
        self.template = template        # Template Sequence Record
        self.target = target            # Target Sequence Record

    # writes alignment to PIR file
    def write_pir(self, filename):
        # Edits description header to be PIR format
        d = self.template.description
        pdb = self.template.id
        code = pdb
        dlist = d.split(":")
        dlist[1] = code
        d = ":".join(dlist)
        self.template.description = d

        print "Writing to %s" % filename
        fileout = open(filename, 'w')
        
        for rec in (self.target, self.template):
            code = rec.id
            d = rec.description
            length = len(rec.seq)
            lines = length/75 + 1

            # start writing
            fileout.write(">P1;%s\n" % code)
            fileout.write("%s\n" % rec.description)
            for i in range(lines):
                fileout.write("%s\n" % rec.seq[i*75:(i+1)*75])
            fileout.write("*\n")
        fileout.close()

    # Edits sequence-structure alignment to match template sequence
    # and edits the header to match the structure
    # Removes overhang in pdb structure too
    def match_aln_to_pdb(self, seqRecord, strRecord, start_res, end_res):
        seqPDB = seqRecord.seq.tomutable()
        strPDB = strRecord.seq.tomutable()
        j = 0
        seq = self.template.seq
        beg = 0
        isbeg = True

        for i, aa in enumerate(seq):

            # not a gap in the template sequence
            if aa != '-':
                while seqPDB[j] == '-':
                    if strPDB[j] != '-':
                        if isbeg:
                            beg += 1
                    j += 1
                isbeg = False
                if seqPDB[j] != '-' and seqPDB[j] != '*' and seqPDB[j] != '':
                    end = 0
                if aa != seqPDB[j]:
                    print 'Error in alignments'
                    print '  %s, %s' % (aa, seqPDB[j])
                if seqPDB[j] != strPDB[j]:
                    seq[i] = '-'
                j += 1
        end = 0
        #j -= 1
        while j < len(strPDB):
            if strPDB[j] != '-':
                end += 1
            j += 1
        self.template.seq = seq
        self.template.description = strRecord.description
        self.set_start_end_residues(start_res + beg, end_res - end)


    # Adds a ligand to the sequence records (".")
    # and edits the record description to reflect change
    def add_blk(self):
        self.template.seq.extend("/.")
        self.target.seq.extend("/.")
        start_res, end_res = self.get_start_end_residues()
        d = self.template.description
        dlist = d.split(":")
        #start_res = int(dlist[2])
        #end_res = int(dlist[4])
        dlist[5] = 'X'
        endali = end_res + 1
        dlist[4] = str(endali)
        d = ":".join(dlist)
        self.template.description = d

    def get_start_end_residues(self):
        d = self.template.description
        dlist = d.split(":")
        start_res = int(dlist[2])
        end_res = dlist[4]
        if end_res.startswith('+'):
            end_res = start_res + int(end_res)
        else:
            end_res = int(end_res)
        return start_res, end_res

    # Update the start and end residues of the template description
    def set_start_end_residues(self, start, end):
        d = self.template.description
        dlist = d.split(":")
        dlist[2] = str(start)
        dlist[4] = str(end)
        self.template.description = ":".join(dlist)

    # Creates a dimer sequence record
    # Input: sequence record, sequence record, code, pair of chain ids
    # Returns: sequence record for chain 1, sequence record for chain 2
    # (Now doesn't take "code", takes code from existing alignment/record)


    # Assumes following format for description
    # separation by ":" 
    # Assumes that the template.code is in the following form: "1PKW"
    # and requires the addition of chain 
    def make_dimer(self, second_aln, tempcode, tarcode, chain):
        dimertemplate = SeqRecord(self.template.seq.toseq(), 
                                  id=(tempcode + chain))
        dimertarget = SeqRecord(self.target.seq.toseq(), id=tarcode, 
                                description=self.target.description)
        dimertemplate.seq = dimertemplate.seq.tomutable()
        dimertarget.seq = dimertarget.seq.tomutable()
        dimertemplate.seq.extend("/")
        dimertemplate.seq.extend(str(second_aln.template.seq))
        dimertarget.seq.extend("/")
        dimertarget.seq.extend(str(second_aln.target.seq))
        startA, endA = self.get_start_end_residues()
        startB, endB = second_aln.get_start_end_residues()

        des = self.template.description
        dlist = des.split(":")
        dlist[1] = dimertemplate.id
        dlist[2] = str(startA)
        dlist[4] = str(endB)
        dlist[5] = chain
        dimertemplate.description = ":".join(dlist)
        aln = MyAlignment(template=dimertemplate, target=dimertarget)
        return aln

    def edit_description(self, tempdes=None, tardes=None):
        if tempdes is None:
            pass
        else:
            self.template.description = tempdes
        if tardes is None:
            pass
        else:
            self.target.description = tardes


# Reads in an alignment file for two sequences and appends the sequence
# of the template structure as the third sequence, aligning it to its
# corresponding fasta sequence, and writes out the new alignment
# Function returns the name of the file write out to and returns
# the starting and ending pdb residue numbers
def make_seq_str_aln(alnfile, tempid, chain, env=None):
    if env is None:
        env = environ()
        env.io.atom_file_directory = ['./']

    start = 'FIRST:' + chain
    end = 'LAST:' + chain
    outfile = tempid + chain + '.pir'
    aln = alignment(env)
    aln.append(file=alnfile, align_codes='all', alignment_format='FASTA')
    mdl = model(env)
    mdl.read(file=tempid, model_segment=(start, end))
    aln.append_model(mdl, align_codes=tempid, atom_files=tempid)
    aln.salign(overhang=30, gap_penalties_1d=(-450, -50),
               align_block=2, align_what='LAST', output='ALIGNMENT')
    aln.write(file=outfile, alignment_format='PIR')
    sequence = aln[2]
    start = sequence.residues[0].num
    end = sequence.residues[-1].num
    return outfile, int(start), int(end)


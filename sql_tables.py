#!/bin/python
import sqlite3

##
# This file contains all of the classes for our database sample, both
# entities and relationships
# adapted by scalhoun from BMI280 class
##

# The Entity base class is just used to hold some common methods
class Entity:
    def __init__(self, cursor):
        self.cursor = cursor

    # This method is just a convenience to avoid a bunch of cutting-and-pasting
    def check_entity(self, table, key, value):
        # Select the record (if there is one)
        query = "SELECT * FROM %s WHERE %s='%s';"%(table,key,value)
        self.cursor.execute(query)
        # OK, now get the row
        row = self.cursor.fetchone()
        if (row == None):
            return -1
        self.id = row[0]
        return self.id


#################################################################################################
# Here are the classes that will hold the entities.

class Target (Entity):
    def __init__(self, cursor, id, num, ligand):
        Entity.__init__(self, cursor)
        self.id = id
        self.num = num
        self.ligand = ligand
        if (self.check_entity("TARGETS", "TARGETID", id) < 0):
            self.cursor.execute("INSERT INTO TARGETS (TARGETID, NUM_BINDING_RES, LIGAND) VALUES ('%s', '%s', '%s');"
                                % (id, num, ligand))
    
    def __str__(self):
        return "Target: %s" % self.id

class Template (Entity):
    def __init__(self, cursor, id, res, state, ligand):
        Entity.__init__(self, cursor)
        self.id = id
        self.res = res
        self.state = state
        self.ligand = ligand
        if (self.check_entity("TEMPLATES", "TEMPLATEID", id) < 0):
            self.cursor.execute("INSERT INTO TEMPLATES (TEMPLATEID, RESOLUTION, BOUNDSTATE, LIGAND) VALUES ('%s', '%s', '%s', '%s');"
                % (id, res, state, ligand))
            self.id = cursor.lastrowid

    def __str__(self):
        return "Template: %s" % self.id

class Model (Entity):
    def __init__(self, cursor, id, zdope, chain, template):
        Entity.__init__(self, cursor)
        self.id = id
        self.zdope = zdope
        self.chain = chain
        self.template = template
        if (self.check_entity("MODELS", "MODELID", id) < 0):
            self.cursor.execute("INSERT INTO MODELS (MODELID, ZDOPE, CHAIN, TEMPLATEID) VALUES ('%s', '%s', '%s', '%s');"
                                % (id, zdope, chain, template))

    def __str__(self):
        return "Model: %s" % self.id

########################## Entity Relationships ###################################

class RMSD:
    def __init__(self, cursor, targetid, modelid, carmsd, mcrmsd, mcrmsd_bs, aarmsd_bs):
        self.targetid = targetid
        self.modelid = modelid
        self.carmsd = carmsd
        self.mcrmsd = mcrmsd
        self.mcrmsd_bs = mcrmsd_bs
        self.aarmsd_bs = aarmsd_bs
        cursor.execute("SELECT * FROM `RMSD` WHERE TARGETID='%s' AND MODELID='%s';" % (targetid, modelid))
        row = cursor.fetchone()
        if (row == None):
            cursor.execute('''
                            INSERT INTO RMSD (TARGETID, MODELID, CALPHA_RMSD, MC_RMSD,
                            BINDSITE_MC_RMSD, BINDSITE_ALL_RMSD) VALUES ('%s', '%s',
                            '%s', '%s', '%s', '%s');''' 
                            % (targetid, modelid, carmsd, mcrmsd, mcrmsd_bs, aarmsd_bs)) 
    
class Homology:
    def __init__(self, cursor, targetid, templateid, seqid_bs, seqid, identities):
        self.targetid = targetid
        self.templateid = templateid
        self.seqid_bs = seqid_bs
        self.seqid = seqid
        self.identities = identities
        cursor.execute("SELECT * FROM `HOMOLOGY` WHERE TARGETID='%s' AND TEMPLATEID='%s';" % (targetid, templateid))
        row = cursor.fetchone()
        if (row == None):
            cursor.execute('''
                            INSERT INTO HOMOLOGY (TARGETID, TEMPLATEID, BINDSITE_SEQID, SEQID, BINDSITE_IDENTITIES)
                            VALUES ('%s', '%s', '%s', '%s', '%s');''' 
                            % (targetid, templateid, seqid_bs, seqid, identities))

class Docking:
    def __init__(self, cursor, modelid, ligand, ligand_pdb, rmsds, energies, zscores):
        self.modelid = modelid
        self.ligand = ligand
        self.ligand_pdb = ligand_pdb
        self.rmsd_best_sampled = rmsds[0]
        self.rmsd_top1 = rmsds[1]
        self.rmsd_top3 = rmsds[2]
        self.rmsd_top10 = rmsds[3]
        self.best_model_energy = energies[0]
        self.top_model_energy = energies[1]
        self.best_model_zscore = zscores[0]
        self.top_model_zscore = zscores[1]
        cursor.execute("SELECT * FROM `DOCKING` WHERE MODELID='%s' AND LIGAND='%s';" % (modelid, ligand))
        row = cursor.fetchone()
        if (row == None):
            cursor.execute('''
                            INSERT INTO DOCKING (MODELID, LIGAND, LIGAND_PDB, RMSD_BEST_SAMPLED, RMSD_TOP1, RMSD_TOP3,
                            RMSD_TOP10, BEST_MODEL_ENERGY, TOP_MODEL_ENERGY) 
                            VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');'''
                            % (modelid, ligand, ligand_pdb, self.rmsd_best_sampled, self.rmsd_top1, self.rmsd_top3,
                            self.rmsd_top10, self.best_model_energy, self.top_model_energy))

class Xtal:
    def __init__(self, cursor, xtalid, ligand, rmsds):
        self.xtalid = xtalid
        self.ligand = ligand
        self.rmsd_best_sampled = rmsds[0]
        self.rmsd_top1 = rmsds[1]
        self.rmsd_top3 = rmsds[2]
        cursor.execute("SELECT * FROM XTAL WHERE XTALID='%s'" % xtalid)
        row = cursor.fetchone()
        if (row == None):
            cursor.execute('''
                INSERT INTO XTAL (XTALID, LIGAND, RMSD_BEST_SAMPLED, RMSD_TOP1, RMSD_TOP3)
                VALUES ('%s', '%s', '%s', '%s', '%s');''' % (xtalid, ligand, self.rmsd_best_sampled, self.rmsd_top1, self.rmsd_top3)
                )




#!/bin/python

# Last edit April 4, 2013

from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.StructureBuilder import StructureBuilder
from Bio.PDB.Structure import Structure
from Bio.PDB import PDBIO, standard_aa_names

class MyStructure(Structure):
    aminoacids = standard_aa_names
    liganddict = {'1VF3':'GDN', '1M9B':'IBG', '3LJR':'GGC', '1BYE':'ATA',
                  '1F3B':'GBX', '3GX0':'GDS', '1B48':'HAG', '1GWC':'GTX',
                  '2F3M':'GTD', '2GSQ':'GBI', '3CSH':'LZ6', '2GST':'GPS',
                  '2AB6':'GSM', '2C4J':'GSO', '3O76':'GTB'}


    def __init__(self, structure=None, pdbid=None):
        self.builder = StructureBuilder()
        if structure is None:
            parser = PDBParser(structure_builder=self.builder)
            structure = parser.get_structure(pdbid, pdbid + '.pdb')
        self.id = structure.id
        self.full_id = structure.full_id
        self.parent = structure.parent
        self.child_list = structure.child_list
        self.child_dict = structure.child_dict
        self.xtra = structure.xtra
        self.model = self.get_list()[0]
        if len(self) > 1:
            print "Note: only first model is processed"


    # Saves pdb structure to output file 'out'
    def save_pdb(self, out):
        print 'Saving %s to %s' % (self.get_id(), out)
        w = PDBIO()
        w.set_structure(self)
        w.save(out)

    # Renumbers residues
    # oldids, residues to be altered
    # newids, new residue numbers
    def renumber_res(self, oldids, newids):
        for chain in self.model:
            for i in range(len(oldids)):
                oldid = oldids[i]
                newid = newids[i]
                res = chain[oldid]
                res.id = (' ', newid, ' ')

    # Removes waters from pdb for first model
    def remove_waters(self):
        for chain in self.model:
            for r in chain.get_unpacked_list():
                id = r.id
                if id[0] == 'W':
                    chain.detach_child(id)

    # Renames given chain to X and residues in that chain to TMP
    # Previously changeHET()
    def rename_het_for_plop(self, chainid):
        chain = self.model[chainid]
        for residue in chain.get_unpacked_list():
            id = residue.id
            residue.resname = 'TMP'
            residue.id = (id[0], 900, id[2])
        model[chainid].id = 'X'    

    # Removes all atoms not in the standard list of amino acids
    # unless in Residue names in keep list
    def remove_het(self, keep):
        for chain in self.model:
            for r in chain.get_unpacked_list():
                id = r.id
                if r.resname not in self.aminoacids and r.resname not in keep:
                    chain.detach_child(id)

    # Takes Chain ID, and list of keep residues
    def remove_het_chain(self, chid, keep):
        chain = self.model[chid]
        for r in chain.get_unpacked_list():
            id = r.id
            if r.resname not in self.aminoacids and r.resname not in keep:
                chain.detach_child(id)

    # Removes all chains except for the ones in list (list of char chain ids)
    def remove_all_except_chains(self, chains):
        for ch in self.model.get_iterator():
            id = ch.get_id()
            if id not in chains:
                self.model.detach_child(id)

    # Given a chain id, returns the sequence
    # Sequence in a list of one-letter amino acid abbreviations
    def chain_to_aa_seq(self, chid):
        chain = self.model[chid]
        residues = chain.get_residues()
        return [Polypeptide.three_to_one(r.resname) for r in residues]

    # takes the chain ID of the molecule that keeps its GSH/ligand
    # and takes a dictionary associating chains with the start residue #
    # and a dictionary associating chains with the end residue #
    def prep_gst(self, chid, start_res, end_res, keepchains):
        self.builder.init_chain('X')
        # self.model.add(builder.init_chain('X'))
        addres = []
        selfaminoacids = standard_aa_names
        ligand = ['GSH']
        ligand.extend(self.liganddict.values())
        ligand.extend(['LZ6', 'GSM', 'GSO', 'GTB'])
        ligand.extend(['EPY', 'GTX', 'GTS', 'GTD', 'GBI', 'HAG'])
        ligand.extend(['GPS', 'GSO', 'GTB', 'GPR', 'GSF', 'GGC'])
        for chain in self.model:
            start = -5
            end = 99999
            chainid = chain.get_id()
            if chainid in keepchains:
                start = start_res[chain.get_id()]
                end = end_res[chain.get_id()]
                print start, end
            for r in chain.get_unpacked_list():
                id = r.id
                # Remove waters
                if id[0] == 'W':
                    chain.detach_child(id)
                # Remove hetatoms
                elif r.resname not in self.aminoacids:
                    if r.resname in ligand and chainid==chid:
                        addres.append(r.copy())
                    chain.detach_child(id)
                # Remove residues not in alignment
		elif (id[1] < start or id[1] > end):
                    print r.resname, r.id[1], chainid
                    chain.detach_child(id)
                elif chainid not in keepchains:
                    chain.detach_child(id)
        # Add GSH from given chain back into structure
        # as it's own chain
        for i in addres:
            self.model['X'].add(i)


    def change_het_to_gsh(self):
        atomdict = {}
        atomdict['GTB'] = ["C'", "C1'", "C2'", "C3'", "C4'", "C5'",
                           "C6'", "N41", "O41", "O42"]
        atomdict['GPR'] = ['CA4', 'CA5', 'CB4', 'CB5', 'CD4', 'CD5',
                           'CG4', 'CG5', 'CE4', 'CE5', 'CZ4', 'CZ5',
                           'CH4', 'CH5', 'O5']
        atomdict['GSO'] = ['C22', 'OF2', 'CM1', 'CM2', 'CL1', 'CL2',
                           'CG2', 'CZ', 'CD2']
        atomdict['GTX'] = ['C1S', 'C2S', 'C3S', 'C4S', 'C5S', 'C6S'] 
        atomdict['GTS'] = ['O1S', 'O2S', 'O3S'] 
        atomdict['HAG'] = ['C41', 'C42', 'C43', 'C44', 'C45', 'C46',
                           'C47', 'C48', 'C49', 'O42', 'O43']
        atomdict['GPS'] = ['CA4', 'CA5', 'CB4', 'CB5', 'CD4', 'CD5',
                           'CG4', 'CG5', 'CE4', 'CE5', 'CZ4', 'CZ5',
                           'CH4', 'CH5', 'O5']
        atomdict['EPY'] = ['C1C', 'C2C', 'C3C', 'C4C', 'C5C', 'C6C',
                           'C7C', 'C8C', 'C9C', 'O2C', 'O3C', 'N8C',
                           'O8C', 'O9C']
        atomdict['GSH'] = [] 

        chain = self.model['C']

        for residue in chain.get_unpacked_list():
            rname = residue.get_resname()
            if rname in atomdict.keys():
                removeatoms = atomdict[rname]
                for atom in residue.get_unpacked_list():
                    name = atom.get_name()
                    id = atom.get_id()
                    if name in removeatoms:
                        residue.detach_child(id)
            else:
                print "Ligand unknown - must manually edit PDB"

            if rname == 'EPY':
                edit_epy(residue)

            residue.resname = 'TMP'
            rid = residue.id
            residue.id = (rid[0], 900, rid[2])

        self.model['C'].id = 'X'

def edit_epy(residue):
    EPYtoGSH = {'O6A':' O31', 'C6A':' C3 ', 'O7A':' O32', 'C5A':' CA3',
                'N4A':' N3 ', 'C3A':' C2 ', 'O3A':' O2 ', 'C2A':' CA2',
                'C1A':' CB2', 'S':' SG2', 'N1B':' N2 ', 'C2B':' CD1', 
                'C3B':' CG1', 'C4B':' CB1', 'C5B':' CA1', 'C6B':' C1 ', 
                'N5B':' N1 ', 'O6B':' O11', 'O7B':' O12', 'O2B':' OE1'}

    for i in EPYtoGSH.keys():
        residue[i].fullname = EPYtoGSH[i]



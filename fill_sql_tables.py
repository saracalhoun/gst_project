#! /usr/bin/python

import sys
import sqlite3
import getopt
import os
from sql_tables import *



#if len(sys.argv) < 4:
#    print "Usage: %s <database file> <analysis input file> <info input file>" % sys.argv[0]
#    sys.exit()
#db = sys.argv[1]
#analysis = sys.argv[2]
#info = sys.argv[3]
def usage():
    print ""
    print "Usage: %s -d <database file>" % os.path.basename(sys.argv[0])
    print ""
    print "Options: "
    print "    -a    <analysis input file>"
    print "    -i    <info input file>"
    print "    -f    <docking output file>"
    print "    -x    <xtal docking file>"

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "d:a:i:f:x:")
    except getopt.GetoptError as err:
        print str(err)
        usage()
        sys.exit(2)
    
    conn = None
    cursor = None
    db = None
    analysis = None
    info = None
    docking = None
    xtal = None
    
    for o, a in opts:
        if o == "-d":
            db = a
        elif o == "-a":
            analysis = a
        elif o == "-i":
            info = a
        elif o == "-f":
            docking = a
        elif o == "-x":
            xtal = a
        else:
            assert False, "unhandled option"

    checkargs = filter(lambda x: x != None, [analysis, info, docking, xtal])
    if db is None or len(checkargs) == 0:
        usage()
        sys.exit(1)

    try:
        conn = sqlite3.connect (db)
        cursor = conn.cursor()
    except sqlite3.Error, e:
        print "Unable to initialize the database: "+e
        sys.exit(1)
    
    if analysis != None:
        try:
            input1 = file(analysis)
        except:
            print "Unable to open analysis input file: %s" % analysis
            sys.exit(1)
        try:
            for line in input1:
                splitline = line.strip().split(",")
                splitline = [elem.strip() for elem in splitline]
                (modelid, chain, targetid, templateid, tarligand, zdope, seqid, carmsd, mcrmsd,
                 mcrmsd_bs, aarmsd_bs, num, iden, seqid_bs) = splitline
                tar = Target(cursor, targetid, num, tarligand)
                model = Model(cursor, modelid, zdope, chain, templateid)
                rmsd = RMSD(cursor, targetid, modelid, carmsd, mcrmsd, mcrmsd_bs, aarmsd_bs)
                homology = Homology(cursor, targetid, templateid, seqid_bs, seqid, iden)
            print "Read analysis input file: %s" % analysis
        except sqlite3.Error, e:
            print "Database error: "+str(e)
            sys.exit(1)
        except:
            print "File read error"
            sys.exit(1)
        input1.close()
    
    if info != None:
        try:
            input2 = file(info)
        except:
            print "Unable to open template information input file: %s" % info
            sys.exit(1)
    
        try:
            for line in input2:
                splitline = line.strip().split(",")
                splitline = [elem.strip() for elem in splitline]
                (templateid, ligand, resolution, state) = splitline
                temp = Template(cursor, templateid, resolution, state, ligand)
            print "Read template information input file: %s" % info
        
        except sqlite3.Error, e:
            print "Database error: "+str(e)
            sys.exit(1)
        except:
            print "File read error"
            sys.exit(1)
        input2.close()

    if docking != None:
        try:
            input3 = file(docking)
        except:
            print "Unable to open docking output file: %s" % docking
            sys.exit(1)

        try:
            for line in input3:
                splitline = line.strip().split(",")
                splitline = [elem.strip() for elem in splitline]
                modelid = splitline[0]
                ligand = splitline[1]
                ligand_pdb = splitline[2]
                rmsds = splitline[3:7]        # best_rmsd_sampled, rmsd_top1, rmsd_top3, rmsd_top10
                energies = splitline[7:9]     # best_model_energy, top_model_energy
                zscores = splitline[9:11]     # best_model_zscore, top_model_zscore

                dock = Docking(cursor, modelid, ligand, ligand_pdb, rmsds, energies, zscores)
            print "Read docking ouput file: %s" % docking

        except sqlite3.Error, e:
            print "Database error: " + str(e)
            sys.exit(1)
        except:
            print "File read error"
            sys.exit(1)
        input3.close()
    if xtal != None:
        try:
            input4 = file(xtal)
        except:
            print "Unable to open xtal docking output file: %s" % xtal
            sys.exit(1)

        try:
            for line in input4:
                splitline = line.strip().split()
                splitline = [elem.strip() for elem in splitline]
                xtalid = splitline[0]
                ligand = splitline[2]
                rmsds = splitline[3:]
                xtaldata = Xtal(cursor, xtalid, ligand, rmsds)
            print "Read xtal docking output file: %s" % xtal
        except sqlite3.Error, e:
            print "Database error: " + str(e)
            sys.exit(1)
        input4.close()
    cursor.close()
    conn.commit()
    conn.close()

if __name__ == "__main__":
    main()

#!/bin/python
'''
Python script that runs modeller's automodel protocol
to build comparative models based on the given sequence alignment
and template structure. Default values are to build 500 models, 
include het atoms, and use DOPE, Normalized DOPE and GA341
assessment scores
'''


import sys, os
from modeller import *
from modeller.automodel import *

if len(sys.argv) < 4:
	print "Usage: python build_model.py <input alignment> <seq id> <template id>"
	sys.exit()

alignment = sys.argv[1].strip()
seq = sys.argv[2].strip()
template = sys.argv[3].strip()

env = environ()
env.io.hetatm = True
env.io.atom_files_directory = ['./', '../']

a = automodel(env, alnfile=alignment,
	knowns=template, sequence=seq,
	assess_methods=(assess.DOPE, assess.normalized_dope, assess.GA341))


a.starting_model = 1
a.ending_model = 500
a.make()

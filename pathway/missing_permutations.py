#!/bin/python

import numpy as np
import networkx as nx
import sys
import os
#import matplotlib.pyplot as plt

def compute_score(v):
    s = 0
    for i in range(len(v)-1):
        s += v[i] * v[i+1]
    return s

order = []
score = []


# from barcave.com/random_hacks/permute.html
def permute(v, start, n, fun, *args):
    if (start == n-1):
        s = fun(v, *args)
        order.append(' '.join(v))
        score.append(s)
    else:
        for i in range(start, n):
            tmp = v[i]
            v[i] = v[start]
            v[start] = tmp
            permute(v, start + 1, n, fun, *args)
            v[start] = v[i]
            v[i] = tmp

def permute_known_end(g, s, t, fun):
    length=g.number_of_nodes()
    paths = nx.all_simple_paths(g, source=s, target=t)
    for path in paths:
        if len(path) == length:
            s = fun(path, g)
            order.append(' '.join(path))
            score.append(s)

def permute_unique(g, v, fun):
    for i in range(len(v)):
        for j in range(i+1, len(v)):
            permute_known_end(g, v[i], v[j], fun)

# Component
class component(object):

    def __init__(self, id, name):
        self.id = id
        self.name = name

# Pairwise data matrix
class dataMatrix(object):
    
    def __init__(self, num):
        self.matrix = np.zeros((num, num))

    def readInData(self, filename):
        handle = open(filename, 'r')
        allcomponents = []

        for line in handle.readlines():
            line = line.split(',')
            line = [each.strip() for each in line]
            
            compid = (None, None)
            if line[0] in allcomponents:
                # get component's id
                #compid[0] = 
                pass
            else:
                compid[0] = len(allcomponents)+1
                comp = component(compid[0], line[0])
                allcomponents.append(line[0])
            if line[1] in allcomponents:
                # get component's id
                pass
            else:
                compid[1] = len(allcomponents)+1
                comp = component(compid[1], line[1])
                allcomponents.append(line[1])

            self.matrix[compid[0]][compid[1]] = line[2]

        handle.close()

    def simData(self):
        x,y = self.matrix.shape
        rmat = np.random.rand(x, y)
        self.matrix = (rmat + rmat.T)/2.

def createGraph(filename, tcfile):
    chemsim = True

    if filename is not None:
        dat = []
        handle = open(filename, 'r')
        for line in handle.readlines():
            line = line.split('\t')
            line = [each.strip() for each in line]
            dat.append(line)
        handle.close()
    else:
        sys.exit()

    G = nx.Graph()
    #cutoff = 50
    cutoff = 100000
    for i in dat:
        if i[0] not in G:
            G.add_node(i[0])
        if i[1] not in G:
            G.add_node(i[1])
        val=float(i[2])
        val= - np.log10(float(i[2]))
        if val > cutoff:
            val = cutoff
        G.add_edge(i[0], i[1], weight=val)

    if chemsim:
        chem = []
        handle = open(tcfile, 'r')
        for line in handle.readlines():
            line = line.split('\t')
            line = [each.strip() for each in line]
            chem.append(line)
        for i in chem:
            if i[0] in G and i[1] in G:
                G.edge[i[0]][i[1]]['TC'] = float(i[2])
        handle.close()
    return G

def score_from_path(path, graph):
    s = 0
    for p in range(len(path)-1):
        edge = graph[path[p]][path[p+1]]['weight']
        s += edge
    return s

def score_from_data(order, m):
    s = 0
    #for i in range(len(order)-1):
    #    s += m.matrix[i][i+1]
    for i in range(len(order)-1):
        x = order[i]-1
        y = order[i+1]-1
        s += - np.log10(m.matrix[x][y])
    return s


def score_from_path_mult(path, graph, seaweight, tcweight):
    s = 0
    for p in range(len(path)-1):
        edge = seaweight*graph[path[p]][path[p+1]]['weight'] + tcweight*graph[path[p]][path[p+1]]['TC']
        s += edge
    return s

def score_from_path_tc(path, graph):
    return score_from_path_mult(path, graph, 0, 1)

def score_from_path_sea(path, graph):
    return score_from_path_mult(path, graph, 1, 0)

def plot_scores(score_np, correct, outfile):
    mu = score_np.mean()
    sigma = score_np.std()
    zscore_np = [(x-mu)/sigma for x in score_np]

    fig = plt.figure(figsize=(7, 7), dpi=200, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(1, 1, 1)
    (n, bins, patches) = ax.hist(zscore_np, 100)
    if correct is not None:
        zcorrect = (correct[1]-mu)/sigma
        y = n[-1]
        for i in range(len(n)-1):
            if zcorrect > bins[i] and zcorrect < bins[i+1]:
                y = n[i]
        ax.annotate('%.3f' % (zcorrect), (zcorrect, y+100), xycoords='data', ha='left', va='bottom')
        ax.axvline(zcorrect, color='red', linestyle='--')
        print zcorrect
    ax.set_xlabel('Z-Score')
    ax.set_ylabel('Frequency')
    fig.tight_layout()
    
    plt.savefig(outfile)
    #plt.show()

if __name__ == "__main__":
    pathway = 'glycolysis'
    scoring = 'sea'
    scoring = 'tc'
    plot = False

    filename = pathway + '_sea.txt' 
    outfile = '%s_%s.png' % (pathway, scoring)
    tcfile = 'tc_' + pathway[0:3] + '.txt'
    stringlist = []
    componentlist = []
    function = None
    if scoring == 'tc':
        function = score_from_path_tc
    if scoring == 'sea':
        function = score_from_path_sea
    if pathway == 'glycolysis':
        componentlist = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    if pathway == 'his':
        componentlist = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    if pathway == 'gmh':
        componentlist = ['1', '3', '4', '5']
        
    graph = createGraph(filename, tcfile)
    results = {}
    results['r'] =[]
    results['z'] = []
    for comp in componentlist:
        order = []
        score = []
        g = graph.copy()
        stringlist = componentlist[:] 
        removelist = [comp]
        #removelist = ['7', '8', '9', '10']
        for r in removelist:
            g.remove_node(r)
            stringlist.remove(r)
        #permute(g.nodes(), 0, g.number_of_nodes(), score_from_path, g)
        #permute_known_end(g, '1', '10')
        permute_unique(g, g.nodes(), function)
    
        num = len(order)
        nra = np.zeros(num, dtype=([('order', '<S20'), ('score', np.float64)]))
        for i, j in enumerate(order):
            nra[i] = (order[i], score[i])
    
        sortar = np.sort(nra, order='score')
        x = sortar[num-11:num]
        for y in range(len(x)):
            print '%s : %.2f' % (x[y][0], x[y][1])
    
        revstringlist = stringlist[::-1]
        string = ' '.join(stringlist)
        revstring = ' '.join(revstringlist)
        elem = np.where(sortar['order'] == string)
        elem2 = np.where(sortar['order'] == revstring)
    
        correct = None
        if len(elem[0]) > 0:
            print ''
            print '%d out of %d' % (num - (elem[0][0]), num)
            print sortar[elem[0][0]]
            correct = sortar[elem[0][0]]
            results['r'].append(num - elem[0][0])
        if len(elem2[0]) > 0:
            print ''
            print '%d out of %d' % (num - (elem2[0][0]), num)
            print sortar[elem2[0][0]]
            correct = sortar[elem2[0][0]]
            results['r'].append(num - elem2[0][0])
        if plot:
            plot_scores(nra['score'], correct, outfile)
        else:
            mu = nra['score'].mean()
            sigma = nra['score'].std()
            zscorecorrect = (correct[1] - mu)/sigma
            results['z'].append(zscorecorrect)
            print zscorecorrect
    
    for res in results['r']:
        print res
    for res in results['z']:
        print '%.3f' % res

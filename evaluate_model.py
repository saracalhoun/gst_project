#!/bin/python

import sys
import os
from structure_edit import *
from modeller import *
from modeller.automodel import *
from modeller.scripts import complete_pdb

def usage():
    print "Usage: evaluate_model.py <alignment file> <model pdb>"
    print "Important: naming convention of alignment file should be"
    print "           alignment file - '2GSR_2c4jA_dimerA.ali'"
    print "           model pdb - '2C4JA_2GSR.pdb'"
    sys.exit()

def main():
    
    if len(sys.argv) < 3:
        usage()

    alignmentfile_fullpath = sys.argv[1]
    modelpdb_fullpath = sys.argv[2]
    
    alignmentfile_dir = os.path.dirname(alignmentfile_fullpath)
    modelpdb_dir = os.path.dirname(modelpdb_fullpath)
    
    if len(alignmentfile_dir) > 0:
        alignmentfile_dir += "/"
    if len(modelpdb_dir) > 0:
        modelpdb_dir += "/"
    
    alignmentfile = os.path.basename(alignmentfile_fullpath)     # '2GSR_2c4jA_dimerA.ali'
    modelpdb = os.path.basename(modelpdb_fullpath)          # '2C4JA_2GSR.pdb'
    
    afilelist = alignmentfile.split('_')
    modelpdblist = modelpdb.split('_')
    
    crystalchain = modelpdblist[0]
    crystal = crystalchain[0:(len(crystalchain)-1)]   # '2C4J'
    chain = crystalchain[len(crystalchain)-1]         # 'A' or 'B'
    template = afilelist[0] + chain                   # '2GSRA'
    tempid = afilelist[0]                             # '2GSR'
    seq = afilelist[1]                                # '2c4jA'
    model = modelpdb.strip('.pdb')                    # '2C4JA_2GSR'
    
    
    distance_cutoff = 4.5     # Define distance cutoff around ligand site
    
    liganddict = {'1VF3':'GDN', '1M9B':'IBG', '3LJR':'GGC', '1BYE':'ATA',
                  '1F3B':'GBX', '3GX0':'GDS', '1B48':'HAG', '1GWC':'GTX',
                  '2F3M':'GTD', '2GSQ':'GBI', '3CSH':'LZ6', '2GST':'GPS',
                  '2AB6':'GSM', '2C4J':'GSO', '3O76':'GTB'}
    
    het = liganddict[crystal]
    
    env = environ()
    env.io.atom_files_directory = [alignmentfile_dir, modelpdb_dir]
    env.io.hetatm = True
    env.libs.topology.read(file='$(LIB)/top_heav.lib')
    env.libs.parameters.read(file='$(LIB)/par.lib')
    
    keepchains = ['A', 'B']
    hetchain = 'A'
    if crystal == '1GWC':
        keepchains = ['B', 'C']
    if chain == 'A':
        hetchain = keepchains[0]
    else:
        hetchain = keepchains[1]

    mdl = complete_pdb(env, model)
    outpdbfile = crystal + chain + '_tmp.pdb'
    myStr = MyStructure(pdbid=(modelpdb_dir+crystal))
    myStr.prep_gst(hetchain, {keepchains[0]: -1, keepchains[1]:-1}, {keepchains[0]: 10000, keepchains[1]: 10000}, keepchains)
    myStr.save_pdb(outpdbfile)
    xtal = complete_pdb(env, outpdbfile)
    os.remove(outpdbfile)
    
    # Model sequence - crystal sequence alignment
    aln1 = alignment(env)
    aln1.append_model(mdl, align_codes=model)
    aln1.append_model(xtal, align_codes=crystal)
    aln1.salign(overhang=30, gap_penalties_1d=(-450, -50),
               output='ALIGNMENT')
    
    # Crystal sequence - model sequence alignment
    aln2 = alignment(env)
    aln2.append_model(xtal, align_codes=crystal)
    aln2.append_model(mdl, align_codes=model)
    aln2.salign(overhang=30, gap_penalties_1d=(-450, -50),
                output='ALIGNMENT')
    
    # Target sequence - template sequence alignment
    aln3 = alignment(env)
    aln3.append(file=alignmentfile_fullpath, align_codes=(seq, template))
    
    # Target sequence - crystal sequence alignment
    aln4 = alignment(env)
    aln4.append(file=alignmentfile_fullpath, align_codes=seq)
    aln4.append_model(xtal, align_codes=crystal)
    aln4.salign(overhang=30, gap_penalties_1d=(-450, -50),
                output='ALIGNMENT')
    
    calpha_super = assess_calpha_rmsd(mdl, xtal, aln1)
    mc_super = assess_mc_rmsd(mdl, xtal, aln1)
    zscore = assess_zdope(mdl)
    seqidentity = assess_seqidentity(aln3)
    sel = select_binding_site(xtal, het, distance_cutoff)
    brmc_super = assess_mc_rmsd_binding_site(mdl, sel, aln2)
    br_super = assess_allatom_rmsd_binding_site(mdl, sel, aln2)
    count, size, ident = assess_seqid_binding_site(sel, aln3, aln4)
    
    print ""
    print ""
    print "Z-Score: %s" % zscore
    print "%% Overall sequence identity between template & model: %s" % seqidentity
    print "C-alpha RMSD between model and xtal structure: %f" % calpha_super.rms
    print "Mainchain RMSD between model and xtal structure: %f" % mc_super.rms
    print "Binding site description - %.1f Angstroms Local Site" % distance_cutoff
    print "Binding site mainchain RMSD: %f" % brmc_super.rms
    print "Binding site all-atom RMSD: %f" % br_super.rms
    print "Binding site residue total count: %d" % size
    print "Binding site residue identities count: %d" % count
    print "Binding site %% sequence identity: %f" % ident
    print ""
    print ""
    print "%s, %s, %s, %s, %s, %f, %f, %f,%f,%f,%f,%d,%d,%f" % (model, chain, crystal, tempid, het, zscore, 
                                                  seqidentity, calpha_super.rms, mc_super.rms, 
                                                  brmc_super.rms, br_super.rms,
                                                  size, count, ident)
    


# Model, Crystal, Model-Xtal alignment
def assess_calpha_rmsd(mdl, xtal, aln1):
    calpha_sel = selection(mdl).only_atom_types('CA').only_std_residues()
    calpha_super = calpha_sel.superpose(xtal, aln1)
    return calpha_super

# Model, Crystal, Model-Xtal alignment
def assess_mc_rmsd(mdl, xtal, aln1):
    mc_sel = selection(mdl).only_mainchain().only_std_residues()
    mc_super = mc_sel.superpose(xtal, aln1)
    return mc_super

# Crystal, ligand name, distance cutoff from ligand
def select_binding_site(xtal, het, distance_cutoff):
    het_sel = selection(xtal).only_residue_types(het)
    bindingres_sel = het_sel.select_sphere(distance_cutoff).by_residue().only_std_residues()
    return bindingres_sel

# Model, binding site selection, Xtal-model alignment 
def assess_mc_rmsd_binding_site(mdl, selection, aln2):
    bindingresmc_sel = selection.only_mainchain()
    brmc_super = bindingresmc_sel.superpose(mdl, aln2)
    return brmc_super

# Model, binding site selection, Xtal-model alignment 
def assess_allatom_rmsd_binding_site(mdl, selection, aln2):
    br_super = selection.superpose(mdl, aln2)
    return br_super

# Model
def assess_zdope(mdl):
    zscore = mdl.assess_normalized_dope()
    return zscore

# Target-template alignment 
def assess_seqidentity(aln3):
    seqidentity = aln3[0].get_sequence_identity(aln3[1])
    return seqidentity

# Binding site selection, target-template alignment, target-xtal alignment
def assess_seqid_binding_site(bindingres_sel, aln3, aln4):
    bsite_xtal = [b.residue for b in bindingres_sel]
    bsite_xtal_index = [b.index for b in bsite_xtal]
    bsite_xtal_index = list(set(bsite_xtal_index))
    bsite_xtal_code = [b.code for b in bsite_xtal]

    aln4_aln3 = {}
    counter = 0
    for res in aln4[0].residues:
        otherres = aln3[0].residues[counter]
        if res.code == otherres.code:
            aln4_aln3[res.index] = otherres.index
        counter += 1;

    bsite_mdl = []
    for res in aln4[1].residues:
        if res.index in bsite_xtal_index:
            alignedres = res.get_aligned_residue(aln4[0])
            if alignedres is None:
                print "No aligned residue between xtal and model: " + res.code + str(res.index)
            else:
                bsite_mdl.append(res.get_aligned_residue(aln4[0]))
    bsite_mdl_index4 = [b.index for b in bsite_mdl]
    
    bsite_mdl_index3 = [aln4_aln3[idx] for idx in bsite_mdl_index4]
    bsite_template = []
    bsite_dict = {}
    for res in aln3[0].residues:
        if res.index in bsite_mdl_index3:
            templateres = res.get_aligned_residue(aln3[1])
            if templateres is None:
                print "No aligned residue between model and template: " + res.code + str(res.index)
            else:
                bsite_template.append(templateres)
                bsite_dict[res.code + str(res.index)] = templateres.code + str(templateres.index)
    bsite_template_index = [b.index for b in bsite_template]

    #count = 0
    identities = {}
    mismatches = {}
    for k in bsite_dict.keys():
        if k[0] == bsite_dict[k][0]:
            #count += 1
            identities[k] = bsite_dict[k]
        else:
            mismatches[k] = bsite_dict[k]
    count = len(identities.keys())
    total = len(bsite_xtal_index)
    bindingsite = 100.0*count/total
    print identities
    print mismatches
    return count, total, bindingsite

if __name__ == "__main__":
    main()

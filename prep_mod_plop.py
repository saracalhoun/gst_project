#!/bin/python

# Last edit April 3, 2013

import sys
import os

if len(sys.argv) < 3:
	print "\tScript edits model pdb file for docking prep"
	print "\tUsage: python prep_mod_plop.py <model pdb file> <output pdb file>"
	sys.exit(0)

from structure_edit import *

filein = sys.argv[1]
fileout = sys.argv[2]
filebase = filein.strip('.pdb')

myStr = MyStructure(pdbid=filebase)
myStr.change_het_to_gsh()
myStr.save_pdb(fileout)


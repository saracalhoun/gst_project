## Scripts and modules for building and analyzing GST models
### Created April 24, 2013

Project repository currently contains a collection of custom python scripts used for generating, analyzing, and preparing homology models. This work was published as part of a larger study here: http://www.ncbi.nlm.nih.gov/pubmed/24802635 (doi: 10.1021/ci5001554)

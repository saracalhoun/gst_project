#!/bin/python

# This script parses the output of hhblits to a fasta formatted 
# pairwise alignment between the input query sequence "Q" and the 
# input sequence/pdb id "T"

import sys
import os
from modeller import *
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio import SeqIO

if len(sys.argv) < 2:
    print "Usage: %s <input prf prf alignment>" % sys.argv[0]
    print ""
    print "Example: %s 3LJR_1JLV_prf_prf.fasta" % sys.arv[0]
    print "From a profile-profile multiple sequence alignment, extracts only the sequences of interest"
    sys.exit(0)

clustalfile = sys.argv[1]

tempid = clustalfile[5:9]
tempid = tempid.upper()
tarid = clustalfile[0:4]
tarid = tarid.upper()
seqid_tar = tarid.lower() + 'A'
seqid_temp = tempid.lower() + 'A'

outfile = "%s_%s.fasta" % (tarid, tempid)
tempfile = "temp_" + outfile

env = environ()
aln = alignment(env)
aln.append(file=clustalfile, align_codes=(seqid_tar, seqid_temp), alignment_format='FASTA')
aln.write(file=tempfile, alignment_format='FASTA')

handle = open(tempfile)
reciter = SeqIO.parse(handle, 'fasta')
records = []
for r in reciter:
    
    if r.id.startswith(seqid_temp):
        r.id = tempid
        r.description = ''
    records.append(r)

SeqIO.write(records, outfile, "fasta")
handle.close()
os.remove(tempfile)



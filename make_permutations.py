#!/bin/python

import numpy as np
import networkx as nx

def compute_score(v):
    s = 0
    for i in range(len(v)-1):
        s += v[i] * v[i+1]
    return s

order = []
score = []


# from barcave.com/random_hacks/permute.html
def permute(v, start, n, fun, *args):
    if (start == n-1):
        s = fun(v, *args)
        order.append(''.join(v))
        score.append(s)
    else:
        for i in range(start, n):
            tmp = v[i]
            v[i] = v[start]
            v[start] = tmp
            permute(v, start + 1, n, fun, *args)
            v[start] = v[i]
            v[i] = tmp

# Component
class component(object):

    def __init__(self, id, name):
        self.id = id
        self.name = name

# Pairwise data matrix
class dataMatrix(object):
    
    def __init__(self, num):
        self.matrix = np.zeros((num, num))

    def readInData(self, filename):
        handle = open(filename, 'r')
        allcomponents = []

        for line in handle.readlines():
            line = line.split(',')
            line = [each.strip() for each in line]
            
            compid = (None, None)
            if line[0] in allcomponents:
                # get component's id
                #compid[0] = 
                pass
            else:
                compid[0] = len(allcomponents)+1
                comp = component(compid[0], line[0])
                allcomponents.append(line[0])
            if line[1] in allcomponents:
                # get component's id
                pass
            else:
                compid[1] = len(allcomponents)+1
                comp = component(compid[1], line[1])
                allcomponents.append(line[1])

            self.matrix[compid[0]][compid[1]] = line[2]

        handle.close()

    def simData(self):
        x,y = self.matrix.shape
        rmat = np.random.rand(x, y)
        self.matrix = (rmat + rmat.T)/2.

def createGraph():
    test = [('1', '2', 0.3), ('2', '3', 0.4), ('1', '3', 0.6), ('2', '4', 0.0009), ('3', '4', 0.0004), ('1', '4', 0.000008)]
    G = nx.Graph()

    for i in test:
        if i[0] not in G:
            G.add_node(i[0])
        if i[1] not in G:
            G.add_node(i[1])
        G.add_edge(i[0], i[1], weight=i[2])
    return G

def score_from_path(path, graph):
    s = 0
    for p in range(len(path)-1):
        edge = graph[path[p]][path[p+1]]['weight']
        s += - np.log10(edge)
    return s

def score_from_data(order, m):
    s = 0
    #for i in range(len(order)-1):
    #    s += m.matrix[i][i+1]
    for i in range(len(order)-1):
        x = order[i]-1
        y = order[i+1]-1
        s += - np.log10(m.matrix[x][y])
    return s
    
if __name__ == "__main__":
    #comp = range(1, 5)
    #m = dataMatrix(len(comp))
    #m.simData()
    #permute(comp, 0, len(comp), score_from_data, m)
    #permute(comp, 0, len(comp), compute_score)
    g = createGraph()
    permute(g.nodes(), 0, g.number_of_nodes(), score_from_path, g)
    #print score_from_path(['1', '2', '3'], g)
    dt = np.dtype([('order', np.str_, 16), ('score', np.float64)])
    darray = np.array([], dtype=dt)
    print order

#!/bin/python

# Last updated 4/1/13 scalhoun
'''
Takes an input alignment and an input PDB structure
For the selected sequence, compares it to the PDB sequence
and removes from the alignment the positions that are not
present in the PDB sequence

Writes out an alignment file with the updated alignments
and a header that is consistent with modeller PIR inputs
'''

import sys
import os

if len(sys.argv) < 4:
    print "   "
    print "Usage: prep_mod_dimer.py <aln file> <template id> <target id>"
    print "   "
    print "Converts ClustalOmega output to modeller-compatible alignment for each dimer"
    print "XXXX_XXXX.fasta -----> XXXX_xxxx_dimerX.ali"
    print "   "
    sys.exit()

from modeller import *
from Bio import SeqIO
from alignment_edit import *
from structure_edit import *

alnfile = sys.argv[1]   # '1YDK_1F3B.fasta', output of clustalOmega
tempid = sys.argv[2]    # '1YDK' template ID
tarid = sys.argv[3]     # '1F3B' target ID
chains = ['A', 'B']
templatechains = ['A', 'B']

if tempid == '2ON5':
    templatechains = ['A', 'G']

seqid = tarid.lower() + 'A'
outali = ['%s_%s_dimer%s.ali' % (tempid, seqid, c) for c in chains]
myAln = {}
stres = {}
endres = {}

env = environ()
env.io.atom_files_directory = ['./']

for tc in templatechains:
    outpir, start_res, end_res = make_seq_str_aln(alnfile, tempid, tc, env)
    handle = open(outpir)
    reciter = SeqIO.parse(handle, 'pir')
    structAln = {}
    for r in reciter:
        if r.description.startswith('structure'):
            structAln['structure'] = r
        else:
            structAln['sequence'] = r
    handle.close()
    handle = open(alnfile)
    alnSeqRecords = SeqIO.parse(handle, 'fasta')
    tempseq = ''
    tarseq = ''
    for s in alnSeqRecords:
        if s.id.startswith(tempid):
            tempseq = s
        elif s.id.startswith(seqid) or s.id.startswith(tarid):
            tarseq = s
        else:
            print "Unknown sequence"
            print s.id
            sys.exit()
    myAln[tc] = MyAlignment(template=tempseq, target=tarseq)
    myAln[tc].match_aln_to_pdb(seqRecord = structAln['sequence'],
                               strRecord = structAln['structure'], start_res=start_res, end_res=end_res)
    myAln[tc].edit_description(tardes=structAln['sequence'].description)
    handle.close() 

    stres[tc], endres[tc] = myAln[tc].get_start_end_residues()

for i, tc in enumerate(templatechains):
    outpdbfile = tempid + chains[i] + '.pdb'
    templatePDB = MyStructure(pdbid=tempid)
    templatePDB.prep_gst(tc, stres, endres, templatechains)
    templatePDB.save_pdb(outpdbfile)

for i, ch in enumerate(chains):
    myDimerAln = myAln[templatechains[0]].make_dimer(
                        second_aln=myAln[templatechains[1]],
                        tempcode=tempid, tarcode=seqid, chain=ch)
    myDimerAln.add_blk()
    myDimerAln.write_pir(outali[i])

